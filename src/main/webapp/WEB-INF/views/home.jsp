<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<html>
<body>
<h1>Buscador animes RIWS</h1>

<form:form modelAttribute="busqueda" action="buscar" method="POST">
Busqueda:<form:input id="search" path="search" />
<form:button value="enviar" >buscar</form:button>
</form:form>

<c:catch var="exception">${busqueda.result}</c:catch>
<%-- <c:if test="${empty exception}">${busqueda.result}</c:if> --%>

	<c:url value="/buscar" var="next">
    <c:param name="pagina" value="${pagina}"/>
    </c:url>
    <a href='<c:out value="/buscar?pagina=${pagina-1}" />' class="pn next">Previous</a>
    |
    <a href='<c:out value="/buscar?pagina=${pagina+1}" />' class="pn next">Next</a>
</body>
</html>