package riws.buscadorAnimes.crowler;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;
import javax.net.ssl.HttpsURLConnection;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

public class Crowler {
	
	public static void run(String url, String locFicheros, int max) throws IOException  {
	    String html = getUrlHTML(url+"/browse");
	    Document doc;
	    String data;
	    borrarArchivo(locFicheros);
		System.out.println("****** Content of AnimeFLV ********");
		do {
			
			data = "";
			doc = Jsoup.parse(html);
			for (Element e:doc.select("article>a")) {
	    		data += parseAnime(url+e.attr("href"), url);
	    		data += "\n";
	    	}
			guardarArchivo(locFicheros, data);
			
			html = url+doc.select("ul.pagination>li").last().child(0).attr("href");
			System.out.println("pagina siguiente: " + html);
			if (!html.contains("#")) {
				if (max < Integer.parseInt(html.split("=")[1])) {
					System.out.println("llegado al limite: " + max);
					return;
				}
			}
			html = getUrlHTML(html);
		} while(!doc.select("ul.pagination>li").last().child(0).attr("href").equals("#"));
	}
	
	private static String parseAnime(String https_url, String url) throws IOException {
		String html = getUrlHTML(https_url);
		Document doc = Jsoup.parse(html);
		String parse = https_url + " @@ ";
		if (doc.select("span.Type ova").size() == 1) { parse += "ova @@ ";	}
		else if (doc.select("span.Type movie").size() == 1) { parse += "movie @@ ";}
		else{parse += "anime @@ ";}
		parse += doc.select("h2.Title").first().text() + " @@ ";
		parse += doc.select("div.Description>p").text() + " @@ ";
		for (Element t:doc.select("Nav.Nvgnrs>a")) {parse +=t.text()+" | ";}	
		parse = parse.substring(0, parse.length()-2);
		parse+=" @@ ";
		parse += doc.select("div.Image>*>img").first().attr("src") + " @@ ";
		parse += doc.select("span.fa-tv").first().text() + " @@ ";
		parse += doc.select("span.vtprmd").first().text() + " @@ ";
		parse += " @#";
		return parse;
	}
	
	private static String getUrlHTML(String https_url) throws IOException {
		URL url = new URL(https_url);
	    HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
	    con.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:62.0) Gecko/20100101 Firefox/62.0");
	    return get_content(con);
	}
	
	private static void guardarArchivo(String nombre, String contenido) throws FileNotFoundException {
		try(FileWriter fw = new FileWriter(nombre, true);
			    BufferedWriter bw = new BufferedWriter(fw);
			    PrintWriter out = new PrintWriter(bw))
		{
		    out.println(contenido);
		    out.flush();
		} catch (IOException e) {
		    e.printStackTrace();
		}
	}
	
	private static void borrarArchivo(String archivo) {
		File file = new File(archivo);
		file.delete();
	}
	
	private static String get_content(HttpsURLConnection con) {
		if (con != null) {
			try {
				BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), "UTF-8"));

				StringBuilder sb = new StringBuilder();
				String input;

				while ((input = br.readLine()) != null) {
					sb.append(input);
				}
				br.close();

				return sb.toString();
			} catch (IOException e) {
				e.printStackTrace();
				return null;
			}

		}
		return null;
	}
}
