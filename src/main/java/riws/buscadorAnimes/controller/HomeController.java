package riws.buscadorAnimes.controller;

import java.io.IOException;
import java.util.List;

import org.apache.lucene.queryparser.classic.ParseException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import riws.buscadorAnimes.main;
import riws.buscadorAnimes.procesado.Buscador;

@Controller
public class HomeController {
	String url = "https://animeflv.net";
	
	private boolean bloqueo = false;
	
	@RequestMapping(value={"/", "/buscar"}, method = RequestMethod.GET)
    public String index(@RequestParam(required = false) Integer pagina, Model model) {
		if (pagina == null) {
			model.addAttribute("busqueda", new Busqueda());
		} else {
			List<List<String>> resultados = null;
	    	try {
	    		//Sacar mas datos
	    		resultados = Buscador.changePage(pagina);
			} catch (IOException e) {
				e.printStackTrace();
			}
	    	
			Busqueda busqueda = new Busqueda();
	    	busqueda.setResult(formatearResultado(resultados));
			
			model.addAttribute("busqueda", busqueda);
		}
        return "home";
    }
    
    @RequestMapping(value="/buscar", method = RequestMethod.POST)
    public String enviar(Busqueda busqueda) {
    	List<List<String>> resultados = null;
    	
    	if (busqueda.getSearch().isEmpty()) {
    		return "";
    	}
    	
    	try {
    		//Sacar mas datos
    		resultados = Buscador.buscador(busqueda.getSearch());
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
    	busqueda.setResult(formatearResultado(resultados));
    	return "home";
    }
    
    private String formatearResultado(List<List<String>> resultados) {
    	if (resultados != null) {
    		if (resultados.isEmpty()) {
    			return "No se han encontrado resultados <br>";
    		} else {
    			StringBuilder sb = new StringBuilder();
    			for (List<String> r : resultados) {
    				//sb.append("<img src=\"" + url + r.get(5) + "\" alt=\"imagen\" width=\"100\"  height=\"100\"");
    				sb.append("<p>");
    				if (r.get(4).equals("En emision")) {
    					sb.append("<div style=\"background-color:#009933;height:10px;width:10px;\"></div>");
    				} else {
    					sb.append("<div style=\"background-color:#800000;height:10px;width:10px;\"/></div>");
    				}
    				sb.append("<b><a href="+r.get(1)+">"+r.get(0)+"</a></b>  ");
                    sb.append("| "+r.get(5) +" | ");
                    sb.append("rating: "+r.get(3)+"</p>");
                    sb.append("<p>"+r.get(2)+"</p>");
                    sb.append("<br>");
				}
    			
    			return sb.toString();
    		}
    	} else {
    		return "error realizando b�squeda <br>";
    	}
    }
    
    @RequestMapping(value="/crowler", method = RequestMethod.GET)
    public String crowlerAndIndex(Model model) {
    	model.addAttribute("busqueda", new Busqueda()); 
        return "crowler";
    }
    
    @RequestMapping(value="/crowler", method = RequestMethod.POST)
    public String ejecutarCrowlerAndIndex(Busqueda busqueda) {
    	if (!bloqueo) {
    		bloqueo = true;
    		main.main(new String[] {});
    		bloqueo = false;
    	} else {
    		return "crowler";
    	}
        return "home";
    }
}
