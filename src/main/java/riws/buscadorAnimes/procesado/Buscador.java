package riws.buscadorAnimes.procesado;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.queryparser.classic.MultiFieldQueryParser;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.Sort;
import org.apache.lucene.search.SortField;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

public class Buscador {
	
	private final static int pageSize = 5;
	
	private static int ActualPage;
	private static IndexSearcher isearcher;
	private static Sort sort;
	private static Query query;
	private static TopDocs topDocs;
	private static List<ScoreDoc> allDocs;
	
	public static List<List<String>> buscador (String busqueda) throws IOException, ParseException {
		Analyzer analyzer = new StandardAnalyzer(Version.LUCENE_48);
		Directory directory = FSDirectory.open(new File("index"));
		
		List<String> normal = new ArrayList<String>(Arrays.asList(busqueda.split(" ")));
		List<String> tags = new ArrayList<String>();
		Iterator<String> i = normal.iterator();
		
		while (i.hasNext()) {
			String s = i.next();
			if (s.substring(0,1).equals("#")) {
				tags.add(s.substring(1));
				i.remove();
			}
		}
		String texto = String.join(" ", normal);
		String tag = String.join(" ", tags);	
		
		DirectoryReader ireader = DirectoryReader.open(directory);
		isearcher = new IndexSearcher(ireader);
		
		System.out.println("Parseando Query " + busqueda);
		if (texto.isEmpty()) {
			QueryParser parser = new QueryParser(Version.LUCENE_48,"tags",analyzer);
			query = parser.parse(tag);
		} else if (tag.isEmpty()) {
			String[] fieldnames =  {"title","descripcion"};
			MultiFieldQueryParser parser = new MultiFieldQueryParser(Version.LUCENE_48,fieldnames,analyzer);
			query = parser.parse(texto);
		} else {
			String[] fieldnames =  {"title","descripcion","tags"};
			String[] queries = {texto,texto,tag};
			query = MultiFieldQueryParser.parse(Version.LUCENE_48,queries,fieldnames,analyzer);
		}		 
		SortField estado = new SortField("estadoS", SortField.Type.STRING);
		SortField rating = new SortField("ratingS", SortField.Type.STRING);
		
		sort = new Sort(new SortField[] {SortField.FIELD_SCORE, estado, rating});
		topDocs = isearcher.search(query, null, pageSize, sort, true, false);
		allDocs = Arrays.asList(topDocs.scoreDocs);
		ActualPage = 1;
		
		System.out.println("Busqueda realizada, " + topDocs.totalHits + " resultados");
		List<List<String>> resultados;
		resultados=showResultados(0, ireader);
		ireader.close();
		directory.close();
		return resultados;
	}
	
	private static List<List<String>> showResultados(int offset, DirectoryReader ireader) throws IOException {
		List<List<String>> results = new ArrayList<List<String>>();
		for (int i=0; i<pageSize; i++) {
			if (offset+i>=allDocs.size()) {
				break;
			}
			List<String> result = new ArrayList<String>();
			Document hitDoc = ireader.document(allDocs.get(offset+i).doc);
			result.add(hitDoc.get("title"));
			result.add(hitDoc.get("link"));
			result.add(hitDoc.get("descripcion"));
			result.add(hitDoc.get("rating"));
			result.add(hitDoc.get("estado"));
			result.add(hitDoc.get("tags"));
			results.add(result);
		}
		return results;
	}
	
	public static List<List<String>> changePage(int page) throws IOException {
		if (topDocs == null) {
			return new ArrayList<List<String>>();
		}
		if (page == 1 && topDocs.scoreDocs.length < pageSize) {
			page = 0;
		}
		ActualPage += page;
		if (ActualPage == 0) {
			ActualPage = 1;
		} 
		FSDirectory directory = FSDirectory.open(new File("index"));
		DirectoryReader ireader = DirectoryReader.open(directory);
		isearcher = new IndexSearcher(ireader);
		//while (topDocs.scoreDocs.length<pageSize*page && topDocs.scoreDocs.length<topDocs.totalHits) {
		while (allDocs.size()<topDocs.totalHits && allDocs.size()<ActualPage*pageSize) {
			List<ScoreDoc> tmp = new ArrayList<ScoreDoc>();
			tmp.addAll(allDocs);
			topDocs = isearcher.searchAfter(topDocs.scoreDocs[topDocs.scoreDocs.length-1], query, pageSize, sort);
			if (topDocs.scoreDocs.length == 0) {break;}
			tmp.addAll(Arrays.asList(topDocs.scoreDocs));
			allDocs = tmp;
			
		}
		System.out.println("pagina: " + ActualPage + " |limite: " + topDocs.totalHits + " |instanciados: " + allDocs.size() + "|a mostrar: " + ActualPage*pageSize);
		List<List<String>> resultados = showResultados((ActualPage-1)*pageSize, ireader);
		ireader.close();
		directory.close();
		return resultados;
	}
}