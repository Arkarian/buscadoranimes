package riws.buscadorAnimes.procesado;

import java.io.File;
import java.io.IOException;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.SortedDocValuesField;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.BytesRef;
import org.apache.lucene.util.Version;

import riws.buscadorAnimes.util.Util;

public class Indexador {
	public static void indexador(String documento) throws IOException, ParseException {

		File rutaIndex = new File("index");
		if (rutaIndex.exists()) {
			Util.borradoRecursivo(rutaIndex);
		}

		Analyzer analyzer = new StandardAnalyzer(Version.LUCENE_48);
		Directory directory = FSDirectory.open(rutaIndex);
		
		String[] fields = {"link","tipo","title","descripcion","tags","imagen","estado","rating"};
		Float[] boost = {0.0F,0.0F,0.8F,0.2F,0.5F,0.0F,0.0F,0.0F};
		IndexWriterConfig config = new IndexWriterConfig(Version.LUCENE_48, analyzer);
		IndexWriter iwriter = new IndexWriter(directory, config);
		String text = Util.leerDoc(documento);
		String[] campos;
		Document doc;
		TextField tmp;
		for (String anime: text.split("@#")) {
			if (anime.length()<10) {continue;}
			doc = new Document();
			campos = anime.split(" @@ ");
			for (int i=0; i<fields.length; i++) {
				System.out.println("indexando: '" + campos[i] + "' en '" + fields[i] + "'");
				tmp = new TextField(fields[i], campos[i], Field.Store.YES);
				tmp.setBoost(boost[i]);
				doc.add(tmp);
			}
			doc.add(new SortedDocValuesField ("estadoS", new BytesRef(campos[campos.length-1])));
			doc.add(new SortedDocValuesField ("ratingS", new BytesRef(campos[campos.length-1])));
			System.out.println("anime indexado: " + campos[2] + "*************************");
			iwriter.addDocument(doc);
		}
		System.out.println("documentos indexado: " + iwriter.numDocs());
		iwriter.close();
		System.out.println("******************cerrado writer*******************");
	}
}
