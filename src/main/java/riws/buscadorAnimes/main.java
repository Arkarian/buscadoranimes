package riws.buscadorAnimes;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Array;

import org.apache.lucene.queryparser.classic.ParseException;

import riws.buscadorAnimes.crowler.Crowler;
import riws.buscadorAnimes.procesado.Indexador;

public class main {

	public static void main(String[] args) {
		
		String https_url = "https://animeflv.net";
		
		String locFicheros = "enlaces\\data.txt";
		
		if (Array.getLength(args) > 0) {
			https_url = args[0];
			locFicheros = args[1];
		}
		
	    try {
	    	//Ejecutar crowler
	    	System.out.println("Ejecución de crowler");
	    	File file = new File(locFicheros);
	    	if (!file.exists()) {
	    		file.mkdirs();
	    	}
	    	Crowler.run(https_url, locFicheros, 200);
	    	
	    	//Ejecutar indexador
	    	System.out.println("Ejecución del indexador");
	    	Indexador.indexador(locFicheros);
	    } catch (IOException e) {
		     e.printStackTrace();
	    } catch (ParseException e) {
			e.printStackTrace();
		}
	}
	
	
}
